package de.paxii.mysql.bridge;

import de.paxii.file.bridge.FileBridge;
import de.paxii.mysql.bridge.config.MySQLDetails;
import de.paxii.mysql.bridge.config.MySQLQueries;
import de.paxii.mysql.bridge.config.MySQLQuery;
import de.paxii.mysql.bridge.config.MySQLQueryType;
import de.paxii.mysql.bridge.jdbc.JDBCConnection;
import de.paxii.mysql.bridge.jdbc.JDBCConnection.Result;
import de.paxii.mysql.bridge.thread.KeepAliveThread;
import de.paxii.mysql.bridge.yaml.ConfigurationSection;
import de.paxii.mysql.bridge.yaml.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import lombok.Getter;

public class MySQLBridge {

  private String dbUser, dbName, dbHost, dbPassword;
  private int dbPort = 3306;
  @Getter
  private JDBCConnection jdbcConnection;
  @Getter
  private FileBridge fileBridge;
  @Getter
  private MySQLQueries queries;

  private KeepAliveThread keepAliveThread;

  public MySQLBridge(Class baseClass) {
    this.fileBridge = new FileBridge();
    this.queries = new MySQLQueries();
    try {
      this.queries.loadFromClass(baseClass);
    } catch (Exception e) {
      e.printStackTrace();
    }

    this.keepAliveThread = new KeepAliveThread(this);
  }

  public void initConnection() {
    this.jdbcConnection = new JDBCConnection(dbHost, dbPort, dbName, dbUser, dbPassword);
    jdbcConnection.open();

    if (jdbcConnection.isConnected()) {
      this.keepAliveThread.start();
    }
  }

  public void setConnectionData(String dbUser, String dbName, String dbHost, int dbPort, String dbPassword) {
    this.dbUser = dbUser;
    this.dbName = dbName;
    this.dbHost = dbHost;
    this.dbPort = dbPort;
    this.dbPassword = dbPassword;
  }

  public void setConnectionData(MySQLDetails mySQLDetails) {
    this.dbHost = mySQLDetails.getHostName();
    this.dbName = mySQLDetails.getDataBase();
    this.dbUser = mySQLDetails.getUserName();
    this.dbPassword = mySQLDetails.getPassword();
    this.dbPort = mySQLDetails.getPort();
  }

  public boolean loadConnectionData(File mySQLData) throws IOException {
    if (!mySQLData.exists()) {
      if (mySQLData.createNewFile()) {
        sout(mySQLData.getAbsolutePath() + " not found, created new one");
      } else {
        sout(mySQLData.getAbsolutePath() + " could not be created.");
      }

      return false;
    }

    if (!mySQLData.getName().endsWith(".yml")) {
      throw new IOException("Invalid Filetype. MySQLBridge only supports YAML.");
    }

    ConfigurationSection configSection = new YamlConfiguration().load(mySQLData);
    this.setConnectionData(
            configSection.getString("username"),
            configSection.getString("database"),
            configSection.getString("host"),
            configSection.getInt("port", 3306),
            configSection.getString("password")
    );

    return true;
  }

  public boolean isConnected() {
    return this.jdbcConnection != null && this.jdbcConnection.isConnected();
  }

  public void closeConnection() {
    if (this.jdbcConnection != null) {
      this.jdbcConnection.close();
    }
  }

  public Result query(String query) {
    if (!this.isConnected())
      this.initConnection();

    return this.jdbcConnection.query(query, true);
  }

  public Result runQuery(String queryName, MySQLQueryType queryType, String... format) {
    return this.runQuery(queryName, queryType, 0, format);
  }

  public Result runQuery(String queryName, MySQLQueryType queryType, int index, String... format) {
    MySQLQuery mySQLQuery = this.queries.getQuery(queryName);

    if (mySQLQuery != null) {
      String[] queries = mySQLQuery.getByType(queryType);

      if (index >= 0 && index < queries.length) {
        String query = queries[index];
        return this.query(String.format(query, (Object[]) format));
      }
    }

    return null;
  }

  public void runAllQueries(String queryName, MySQLQueryType queryType) {
    MySQLQuery mySQLQuery = this.queries.getQuery(queryName);

    if (mySQLQuery != null) {
      String[] queries = mySQLQuery.getByType(queryType);
      Arrays.stream(queries).forEachOrdered(this::query);
    }
  }

  public boolean doesTableExist(String tableName) {
    ResultSet resultSet = this.query(String.format("SHOW TABLES FROM %s;", this.dbName)).getResultSet();

    try {
      if (!resultSet.isAfterLast() && resultSet.first()) {
        while (!resultSet.isAfterLast()) {
          if (resultSet.getString("Tables_in_" + this.dbName).equals(tableName)) {
            return true;
          }
          resultSet.next();
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return false;
  }

  private static void sout(String message) {
    System.out.println("[MySQLBridge] " + message);
  }

}
