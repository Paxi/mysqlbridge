package de.paxii.mysql.bridge.config;

/**
 * Created by Lars on 26.12.2016.
 */
public enum MySQLQueryType {
  SELECT,
  INSERT,
  UPDATE,
  DELETE,
  CREATE;
}
