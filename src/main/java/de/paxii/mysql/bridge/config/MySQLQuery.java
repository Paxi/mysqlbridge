package de.paxii.mysql.bridge.config;

import de.paxii.mysql.bridge.yaml.ConfigurationSection;

import lombok.Data;

/**
 * Created by Lars on 25.12.2016.
 */
@Data
public class MySQLQuery {

  private final String name;
  private final String[] select;
  private final String[] insert;
  private final String[] update;
  private final String[] delete;
  private final String[] create;

  public static MySQLQuery fromConfigurationSection(String queryName, ConfigurationSection configSection) {
    return new MySQLQuery(
            queryName,
            getQueryFromConfigurationSection(configSection, "select"),
            getQueryFromConfigurationSection(configSection, "insert"),
            getQueryFromConfigurationSection(configSection, "update"),
            getQueryFromConfigurationSection(configSection, "delete"),
            getQueryFromConfigurationSection(configSection, "create")
    );
  }

  private static String[] getQueryFromConfigurationSection(ConfigurationSection configSection, String name) {
    if (configSection.isString(name)) {
      return new String[]{configSection.getString(name)};
    }

    return configSection.getStringList(name).stream().toArray(String[]::new);
  }

  public String[] getByType(MySQLQueryType queryType) {
    switch (queryType) {
      case CREATE:
        return this.create;
      case SELECT:
        return this.select;
      case INSERT:
        return this.insert;
      case UPDATE:
        return this.update;
      case DELETE:
        return this.delete;
    }

    return new String[0];
  }

}
