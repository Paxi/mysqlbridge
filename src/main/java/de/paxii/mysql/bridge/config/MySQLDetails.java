package de.paxii.mysql.bridge.config;

import lombok.Getter;
import lombok.Setter;

public class MySQLDetails {
  @Getter
  @Setter
  private String hostName;
  @Getter
  @Setter
  private String userName;
  @Getter
  @Setter
  private String password;
  @Getter
  @Setter
  private String dataBase;
  @Getter
  @Setter
  private int port = 3306;

  public MySQLDetails(String hostName, String userName, String password, String dataBase, int port) {
    this.hostName = hostName;
    this.userName = userName;
    this.password = password;
    this.dataBase = dataBase;
    this.port = port;
  }
}
