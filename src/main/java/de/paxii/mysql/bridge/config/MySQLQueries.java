package de.paxii.mysql.bridge.config;

import de.paxii.mysql.bridge.MySQLBridge;
import de.paxii.mysql.bridge.yaml.ConfigurationSection;
import de.paxii.mysql.bridge.yaml.YamlConfiguration;

import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by Lars on 25.12.2016.
 */
public class MySQLQueries {
  private HashMap<String, MySQLQuery> queryHashMap;

  public MySQLQueries() {
    this.queryHashMap = new HashMap<>();
  }

  public void loadFromClass(Class baseClass) {
    InputStream inputStream = baseClass.getClassLoader().getResourceAsStream("queries.yml");

    if (inputStream == null) {
      System.out.println("[MySQLBridge] Could not find queries.yml for base class " + baseClass.getCanonicalName());
      return;
    }
    ConfigurationSection yamlConfiguration = new YamlConfiguration().load(inputStream);
    ConfigurationSection configSection = yamlConfiguration.getConfigurationSection("queries");
    configSection.getKeys().forEach(queryName -> {
      ConfigurationSection querySection = configSection.getConfigurationSection(queryName);
      this.addQuery(MySQLQuery.fromConfigurationSection(queryName, querySection));
    });
  }

  public void addQuery(MySQLQuery mySQLQuery) {
    this.queryHashMap.put(mySQLQuery.getName(), mySQLQuery);
  }

  public MySQLQuery getQuery(String queryName) {
    return this.queryHashMap.get(queryName);
  }

  public String[] getSelect(String queryName) {
    if (this.queryHashMap.containsKey(queryName)) {
      return this.queryHashMap.get(queryName).getSelect();
    }

    return null;
  }

  public String[] getInsert(String queryName) {
    if (this.queryHashMap.containsKey(queryName)) {
      return this.queryHashMap.get(queryName).getInsert();
    }

    return null;
  }

  public String[] getUpdate(String queryName) {
    if (this.queryHashMap.containsKey(queryName)) {
      return this.queryHashMap.get(queryName).getUpdate();
    }

    return null;
  }

  public String[] getDelete(String queryName) {
    if (this.queryHashMap.containsKey(queryName)) {
      return this.queryHashMap.get(queryName).getDelete();
    }

    return null;
  }

  public String[] getCreate(String queryName) {
    if (this.queryHashMap.containsKey(queryName)) {
      return this.queryHashMap.get(queryName).getCreate();
    }

    return null;
  }

}
