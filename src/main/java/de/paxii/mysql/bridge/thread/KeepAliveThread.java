package de.paxii.mysql.bridge.thread;

import de.paxii.mysql.bridge.MySQLBridge;

/**
 * Created by Lars on 15.09.2015.
 */
public class KeepAliveThread extends Thread {
  private MySQLBridge mySQLBridge;

  public KeepAliveThread(MySQLBridge mySQLBridge) {
    this.mySQLBridge = mySQLBridge;
  }

  @Override
  public void run() {
    while (true) {
      if (!this.mySQLBridge.isConnected()) {
        this.mySQLBridge.initConnection();
      }
      try {
        Thread.sleep(30000L);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
