package de.paxii.mysql.bridge.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lombok.Getter;

public class JDBCConnection {
  @Getter
  public Connection connection = null;
  private String driver;
  private String connectionString;

  public JDBCConnection(String hostname, int port, String database, String username, String password) {
    driver = "com.mysql.jdbc.Driver";
    connectionString = "jdbc:mysql://" + hostname + ":" + port + "/"
            + database + "?user=" + username + "&password=" + password;
  }

  public Connection open() {
    try {
      Class.forName(driver);
      this.connection = DriverManager.getConnection(connectionString);
      return connection;
    } catch (SQLException e) {
      System.out.println("Could not connect to Database! because: " + e.getMessage());
    } catch (ClassNotFoundException e) {
      System.out.println("NO DRIVER FOUND!");
      e.printStackTrace();
    }
    return this.connection;
  }

  @Deprecated
  public Connection getConn() {
    return this.connection;
  }

  public void close() {
    try {
      if (connection != null)
        connection.close();
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    connection = null;
  }

  public boolean isConnected() {
    try {
      return (!(connection == null || connection.isClosed()));
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

  public Result query(final String query) {
    return query(query, true);
  }

  public Result query(final String query, boolean retry) {
    try {
      PreparedStatement statement = null;
      try {
        if (!isConnected())
          open();
        statement = connection.prepareStatement(query);
        if (statement.execute())
          return new Result(statement, statement.getResultSet());
      } catch (final SQLException e) {
        final String msg = e.getMessage();
        if (retry && msg.contains("_BUSY")) {
          new Thread(() -> {
            try {
              Thread.sleep(200L);
            } catch (InterruptedException e1) {
              e1.printStackTrace();
            }
            query(query, false);
          }).start();
        } else {
          e.printStackTrace();
        }
      }
      if (statement != null)
        statement.close();
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  protected Statements getStatement(String query) {
    String trimmedQuery = query.trim();
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("SELECT"))
      return Statements.SELECT;
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("INSERT"))
      return Statements.INSERT;
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("UPDATE"))
      return Statements.UPDATE;
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("DELETE"))
      return Statements.DELETE;
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("CREATE"))
      return Statements.CREATE;
    if (trimmedQuery.substring(0, 5).equalsIgnoreCase("ALTER"))
      return Statements.ALTER;
    if (trimmedQuery.substring(0, 4).equalsIgnoreCase("DROP"))
      return Statements.DROP;
    if (trimmedQuery.substring(0, 8).equalsIgnoreCase("TRUNCATE"))
      return Statements.TRUNCATE;
    if (trimmedQuery.substring(0, 6).equalsIgnoreCase("RENAME"))
      return Statements.RENAME;
    if (trimmedQuery.substring(0, 2).equalsIgnoreCase("DO"))
      return Statements.DO;
    if (trimmedQuery.substring(0, 7).equalsIgnoreCase("REPLACE"))
      return Statements.REPLACE;
    if (trimmedQuery.substring(0, 4).equalsIgnoreCase("LOAD"))
      return Statements.LOAD;
    if (trimmedQuery.substring(0, 7).equalsIgnoreCase("HANDLER"))
      return Statements.HANDLER;
    if (trimmedQuery.substring(0, 4).equalsIgnoreCase("CALL")) {
      return Statements.CALL;
    }
    return Statements.SELECT;
  }

  protected enum Statements {
    SELECT,
    INSERT,
    UPDATE,
    DELETE,
    DO,
    REPLACE,
    LOAD,
    HANDLER,
    CALL,
    CREATE,
    ALTER,
    DROP,
    TRUNCATE,
    RENAME,
    START,
    COMMIT,
    ROLLBACK,
    SAVEPOINT,
    LOCK,
    UNLOCK,
    PREPARE,
    EXECUTE,
    DEALLOCATE,
    SET,
    SHOW,
    DESCRIBE,
    EXPLAIN,
    HELP,
    USE,
    ANALYZE,
    ATTACH,
    BEGIN,
    DETACH,
    END,
    INDEXED,
    ON,
    PRAGMA,
    REINDEX,
    RELEASE,
    VACUUM;
  }

  public class Result {
    private ResultSet resultSet;
    private Statement statement;

    public Result(Statement statement, ResultSet resultSet) {
      this.statement = statement;
      this.resultSet = resultSet;
    }

    public ResultSet getResultSet() {
      return this.resultSet;
    }

    public void close() {
      try {
        this.statement.close();
        this.resultSet.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
