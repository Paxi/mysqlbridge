package de.paxii.mysql.bridge.list;

public class Query {
  private String queryName;
  private String queryString;

  public Query(String queryName, String queryString) {
    this.queryName = queryName;
    this.queryString = queryString;
  }

  public String getName() {
    return this.queryName;
  }

  public void setName(String queryName) {
    this.queryName = queryName;
  }

  public String getQuery() {
    return this.queryString;
  }

  public void setQuery(String queryString) {
    this.queryString = queryString;
  }

  public String formatQuery(Object... args) {
    return String.format(getQuery(), args);
  }
}
