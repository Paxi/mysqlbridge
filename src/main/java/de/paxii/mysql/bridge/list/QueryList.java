package de.paxii.mysql.bridge.list;

import de.paxii.mysql.bridge.MySQLBridge;

import java.io.File;
import java.util.ArrayList;

public class QueryList {
  private MySQLBridge mysqlBridge;
  private ArrayList<Query> queryList;

  public QueryList(MySQLBridge mysqlBridge) {
    this.mysqlBridge = mysqlBridge;
    this.queryList = new ArrayList<Query>();
  }

  public boolean addQuery(Query query) {
    if (!doesQueryExist(query.getName())) {
      this.queryList.add(query);
      return true;
    }

    return false;
  }

  public boolean removeQuery(Query query) {
    if (doesQueryExist(query.getName())) {
      this.queryList.remove(query);
      return true;
    }

    return false;
  }

  public boolean doesQueryExist(String queryName) {
    for (Query query : queryList) {
      if (query.getName().equals(queryName)) {
        return true;
      }
    }

    return false;
  }

  public void addFolder(File folderPath) {
    if (folderPath.isDirectory()) {
      for (File folderFile : folderPath.listFiles()) {
        if (folderFile.getName().endsWith(".sql")) {
          String queryName = folderFile.getName().substring(0, folderFile.getName().length() - 5);
          String queryString = "";

          ArrayList<String> fileLines = this.mysqlBridge.getFileBridge().loadFile(folderFile);

          if (fileLines.size() > 0) {
            for (String fileLine : fileLines) {
              queryString += fileLine + " ";
            }

            queryString = queryString.substring(0, queryString.length() - 2);

            Query query = new Query(queryName, queryString);
            addQuery(query);
          } else {
            continue;
          }
        }
      }
    }
  }
}
